<?php
// $id$
/**
 * @file 
 * 
 * add a child of a content type to a selected node. 
 *   1. Identify the BID of the last entity created by this user. 
 *   2. Create new content of type selected. Call an action -->
 *          --> Entities-->Create a new Entity -->
 *   3. Identify the NID of the last entity created by this user. 
 *   4. Add the BID number of the first node to the BID of the second. 
*/
/**
 * Impliments hook_rules_action_info()
 * Declares any meta-data about the actions for rules.
 */

function add_child_rules_action_info() {
  $parent_nid;
  $act = array(
       'get_parent_nid_action' => array(
          'label' => t('Identify Parent of Book'), // Name displayed to admins
          'group' => t('Add a child page'), // Used for grouping actions in select lists
             'parameter' => array(
               'node' => array('type' => 'node', 'label' => t('Content')),
             ),
        ),
        'add_child_action' => array(
          'label' => t('Make last entity a child page'), // Name displayed to admins
          'group' => t('Add a child page'), // Used for grouping actions in select lists
             'parameter' => array(
               'node' => array('type' => 'node', 'label' => t('Content')),
             ),
        ),  
  );
  return $act;
}

/*
 * A function to select the $node and the $parent
 */
function get_parent_nid_action($node) {  
  global $parent_nid;   //create a variable to store the parent bid.
  $parent_nid = $node->book['bid'];  // save the parent bid in the variable. 
  return $parent_nid;
}

function add_child_action($node) { 
  global $parent_nid;        //
  $node->book['bid'] = $parent_nid;  //assign the saved parent nid to the  entity-content created.  
}