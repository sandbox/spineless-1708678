
--------------------------------------------------------------------------------
                                 Rule Add child module
--------------------------------------------------------------------------------

Maintainers:
 * Chris Pinter - www.pinterec.ca,  Drupal Avator=Spineless

This module will add a child page under a newly created node.


Installation
------------

 * Add Child depends on the rules module, book and  book_made_simple.


Documentation
-------------
You are looking at the documentation.

How to use this module
------------

The intention of this module is to add a child page to a newly created book page
using book made simple.  Create your new content type and configure book made simple
to make the node a book. 

You will also need to create a new content type that will be the child page.

Then configure add child module.

STEP 1 - CREATE A RULES COMPONENT (one time setup)

This step puts 3 rules together that are always repeated for each new child added.

1. Go to Admin/Configuration/workfow/rules/components/import component
2. Copy the following code and place it in the text window so it can be imported.

 { "rules_save_new_entity_as_a_child" : {
    "LABEL" : "Save new entity as a child",
    "PLUGIN" : "action set",
    "REQUIRES" : [ "rules", "add_child" ],
    "USES VARIABLES" : { "entity_just_created" : { "label" : "Entity just created [entity-created]", "type" : "node" } },
    "ACTION SET" : [
      { "entity_save" : { "data" : [ "entity-just-created" ], "immediate" : 1 } },
      { "add_child_action" : { "node" : [ "entity-just-created" ] } },
      { "entity_save" : { "data" : [ "entity-just-created" ], "immediate" : 1 } }
    ]
  }
}

STEP 2 - Create the rule to add a child page to the parent page.

1. Go to Admin/Configuration/workflow/rules
2. Add a new rule of type " After Saving new Content".
3. In the CONDITIONS section you need set the following minimim conditions. 
 (i)Content is of type  ->  Content[node] , 
 _ _ _ _ _ _ _ _ _ _ _ _ _Content [content type of parent page],
 (ii) Data Comparission ->  Data to compare:node:[field variable],
 __ _ _ _ _ _ _ _ _ _ _ _ _ _Data value:  [value of field variable],
                        
4. ACTION  (VERY IMPORTANT STUFF HERE)
(i)  Identify Parent of Book     //Must be selected first to save the book identity of the parent.
  -
(ii) Create a new entity ->  Entity type[Node],
  __ _ _ _ _ _ _ _ _ _ _ _ _ _ _Content type: [the content type of the child page],
  __ _ _ _ _ _ _ __ _ _ _ _ _ _Author: [node:author]
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _Provided variables: [Created entity (entity_created),
  -
(iii)Save a new entity as a child -> Entity just created: [entity-created]
